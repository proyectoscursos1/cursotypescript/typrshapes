class Shape {
    constructor(public ctx: CanvasRenderingContext2D, public x: number, public y: number) {
    }
}

class Line extends Shape {
    constructor(ctx: CanvasRenderingContext2D,
        public originX: number,
        public originY: number,
        x: number,
        y: number) {
        super(ctx, x, y);
    }

    draw(): void {
        this.ctx.moveTo(this.originX, this.originY);
        this.ctx.lineTo(this.x, this.y);
        this.ctx.stroke();
    }
}

class Circle extends Shape {
    constructor(ctx: CanvasRenderingContext2D,
        public radius: number,
        public startAngle: number,
        public endAngle: number,
        x: number,
        y: number) {
        super(ctx, x, y);
    }

    stroke(): void {
        this.ctx.arc(this.x, this.y, this.radius, this.startAngle, this.endAngle);
    }
}
class MathHelper {
    static generateRandom(): number {
        return Math.random() * 500;
    }
}

const btnLine = document.getElementById('drawLine');
btnLine?.addEventListener('click', () => {
    const c = <HTMLCanvasElement>document.getElementById('myCanvas');
    const ctx = <CanvasRenderingContext2D>c.getContext('2d');

    const line = new Line(ctx, MathHelper.generateRandom(), MathHelper.generateRandom(), MathHelper.generateRandom(), MathHelper.generateRandom());
    line.draw();

    const circle = <HTMLCanvasElement>document.getElementById('myCanvas');
    const ctx1 = <CanvasRenderingContext2D>circle.getContext('2d');

    ctx1.beginPath();
    ctx1.arc(MathHelper.generateRandom(), MathHelper.generateRandom(), MathHelper.generateRandom(), MathHelper.generateRandom(), MathHelper.generateRandom() * Math.PI);
    ctx1.stroke();
});