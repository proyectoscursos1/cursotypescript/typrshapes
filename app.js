"use strict";
class Shape {
    constructor(ctx, x, y) {
        this.ctx = ctx;
        this.x = x;
        this.y = y;
    }
}
class Line extends Shape {
    constructor(ctx, originX, originY, x, y) {
        super(ctx, x, y);
        this.originX = originX;
        this.originY = originY;
    }
    draw() {
        this.ctx.moveTo(this.originX, this.originY);
        this.ctx.lineTo(this.x, this.y);
        this.ctx.stroke();
    }
}
class Circle extends Shape {
    constructor(ctx, radius, startAngle, endAngle, x, y) {
        super(ctx, x, y);
        this.radius = radius;
        this.startAngle = startAngle;
        this.endAngle = endAngle;
    }
    stroke() {
        this.ctx.arc(this.x, this.y, this.radius, this.startAngle, this.endAngle);
    }
}
class MathHelper {
    static generateRandom() {
        return Math.random() * 500;
    }
}
const btnLine = document.getElementById('drawLine');
btnLine === null || btnLine === void 0 ? void 0 : btnLine.addEventListener('click', () => {
    const c = document.getElementById('myCanvas');
    const ctx = c.getContext('2d');
    const line = new Line(ctx, MathHelper.generateRandom(), MathHelper.generateRandom(), MathHelper.generateRandom(), MathHelper.generateRandom());
    line.draw();
    const circle = document.getElementById('myCanvas');
    const ctx1 = circle.getContext('2d');
    ctx1.beginPath();
    ctx1.arc(MathHelper.generateRandom(), MathHelper.generateRandom(), MathHelper.generateRandom(), MathHelper.generateRandom(), MathHelper.generateRandom() * Math.PI);
    ctx1.stroke();
});
